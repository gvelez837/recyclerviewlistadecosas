package com.example.recyclerviewgemavelez;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recycle_view);

        ArrayList<String> tareas = new ArrayList<>();

        for (int item = 0; item<=0; item++){
            tareas.add("LISTA DE COSA");
            tareas.add("lapiz " );
            tareas.add("pluma " );
            tareas.add("borrador" );
            tareas.add("resaltado");
            tareas.add("regla "  );
            tareas.add("hojas bom" );
            tareas.add("mochila " );
            tareas.add("zapatos" );
            tareas.add("silla " );
            tareas.add("corrector " );
            tareas.add("cuaderno" );
            tareas.add("regla");
            tareas.add("pluma " );
        }


        TareasRecyclerViewAdapter adapter = new TareasRecyclerViewAdapter( this, tareas);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager( this));

    }
}